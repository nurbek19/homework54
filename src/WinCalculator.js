export const OUTCOMES = {
    ROYAL_FLUSH: 'Royal Flush',
    FLUSH: 'Flush',
    PAIR: 'A One Pair',
    TWO_PAIR: 'A Two Pair',
    THREE_PAIR: 'A Three Pair',
    POKER: 'Poker',
    FULL_HOUSE: 'Full House',
    STRAIGHT_FLUSH: 'Straight Flush',
    NOTHING: 'Nothing'
};

class WinCalculator {
    constructor(cards) {
        this.cards = cards;
        this.suits = this.cards.map(card => card.suit);
        this.ranks = this.cards.map(card => card.rank);
        this.isFlush = this.suits.every(suit => suit === this.suits[0]);
    }

    isRoyalFlush() {
        return this.isFlush &&
            this.ranks.includes('10') &&
            this.ranks.includes('J') &&
            this.ranks.includes('Q') &&
            this.ranks.includes('K') &&
            this.ranks.includes('A')
    }

    isStraightFlush() {
        if (this.suits.every(suit => suit === this.suits[0])) {
            let straightFlushArray = [];
            let straightSuitAmount = 0;

            for(let i = 0; i < this.ranks.length; i++) {
                if(this.ranks[i] === 'J') {
                    straightFlushArray.push('11');
                } else if(this.ranks[i] === 'Q') {
                    straightFlushArray.push('12');
                } else if(this.ranks[i] === 'K') {
                    straightFlushArray.push('13');
                } else {
                    straightFlushArray.push(this.ranks[i]);
                }
            }

            function compareNumbers(a, b) {
                return a - b;
            }

            straightFlushArray.sort(compareNumbers);

            for(let i = 0; i < straightFlushArray.length; i++) {
                if((straightFlushArray[i + 1] - straightFlushArray[i]) === 1) {
                    straightSuitAmount++;
                }
            }

            console.log(straightFlushArray);
            console.log(straightSuitAmount);

            return straightSuitAmount;
        }
    }

    isPair() {
        const ranksNumber = {};
        let pairIndex = 0;

        this.ranks.forEach(rank => {
            if (!ranksNumber[rank]) {
                ranksNumber[rank] = 1;
            } else {
                ranksNumber[rank]++;
            }
        });

        const rankValues = Object.values(ranksNumber);

        rankValues.forEach(value => {
            if (value === 2) {
                pairIndex++;
            }
        });


        if (rankValues.includes(3) && rankValues.includes(2)) {
            return 'full house';
        } else if (rankValues.includes(3)) {
            return 'three pair';
        } else if (pairIndex === 1) {
            return 'one pair';
        } else if (pairIndex === 2) {
            return 'two pair'
        } else if (rankValues.includes(4)) {
            return 'poker';
        }
    }

    getBestHand() {
        if (this.isRoyalFlush()) {
            return OUTCOMES.ROYAL_FLUSH;
        } else if (this.isStraightFlush() === 4) {
            return OUTCOMES.STRAIGHT_FLUSH;
        } else if (this.isFlush) {
            return OUTCOMES.FLUSH;
        } else if (this.isPair() === 'one pair') {
            return OUTCOMES.PAIR;
        } else if (this.isPair() === 'two pair') {
            return OUTCOMES.TWO_PAIR;
        } else if (this.isPair() === 'three pair') {
            return OUTCOMES.THREE_PAIR;
        } else if (this.isPair() === 'poker') {
            return OUTCOMES.POKER;
        } else if (this.isPair() === 'full house') {
            return OUTCOMES.FULL_HOUSE;
        } else {
            return OUTCOMES.NOTHING;
        }
    }
}

export default WinCalculator;

