import WinCalculator, { OUTCOMES } from "./WinCalculator";

const royalFlush = [
    {suit: 'H', rank: 'Q'},
    {suit: 'H', rank: 'A'},
    {suit: 'H', rank: 'J'},
    {suit: 'H', rank: 'K'},
    {suit: 'H', rank: '10'}
];

const flush = [
    {suit: 'H', rank: '2'},
    {suit: 'H', rank: '8'},
    {suit: 'H', rank: 'A'},
    {suit: 'H', rank: '5'},
    {suit: 'H', rank: 'K'}
];

const pair = [
    {suit: 'H', rank: 'Q'},
    {suit: 'C', rank: 'A'},
    {suit: 'S', rank: '2'},
    {suit: 'D', rank: 'K'},
    {suit: 'H', rank: '2'}
];

const twoPair = [
    {suit: 'H', rank: 'Q'},
    {suit: 'C', rank: 'K'},
    {suit: 'S', rank: '2'},
    {suit: 'D', rank: 'K'},
    {suit: 'H', rank: '2'}
];

const threePair = [
    {suit: 'H', rank: 'Q'},
    {suit: 'C', rank: '2'},
    {suit: 'S', rank: '2'},
    {suit: 'D', rank: 'K'},
    {suit: 'H', rank: '2'}
];

const poker = [
    {suit: 'H', rank: '2'},
    {suit: 'C', rank: '2'},
    {suit: 'S', rank: '2'},
    {suit: 'D', rank: 'K'},
    {suit: 'D', rank: '2'}
];

const fullHouse = [
    {suit: 'H', rank: '2'},
    {suit: 'C', rank: '2'},
    {suit: 'S', rank: '2'},
    {suit: 'D', rank: 'K'},
    {suit: 'C', rank: 'K'}
];

const straightFlush = [
    {suit: 'H', rank: '9'},
    {suit: 'H', rank: 'J'},
    {suit: 'H', rank: '10'},
    {suit: 'H', rank: 'K'},
    {suit: 'H', rank: 'Q'}
];

it('should determine royal flush', () => {
    const calc = new WinCalculator(royalFlush);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.ROYAL_FLUSH);
});

it('should determine flush', () => {
    const calc = new WinCalculator(flush);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.FLUSH);
});

it('should determine a pair', () => {
    const calc = new WinCalculator(pair);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.PAIR);
});

it('should determine a two pair', () => {
    const calc = new WinCalculator(twoPair);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.TWO_PAIR);
});

it('should determine a three pair', () => {
    const calc = new WinCalculator(threePair);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.THREE_PAIR);
});

it('should determine a poker', () => {
    const calc = new WinCalculator(poker);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.POKER);
});

it('should determine a full house', () => {
    const calc = new WinCalculator(fullHouse);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.FULL_HOUSE);
});

it('should determine a straight flush', () => {
    const calc = new WinCalculator(straightFlush);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.STRAIGHT_FLUSH);
});

