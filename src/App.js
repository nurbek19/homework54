import React, {Component} from 'react';
import './App.css';
import Card from './card';
import WinCalculator from './WinCalculator';

class App extends Component {
    state = {
        array: [],
        outcome: ''
    };

    handleClick = () => {
        let array = [];
        let arrOfRank = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
        let arrOfSuit = ['diams', 'hearts', 'clubs', 'spades'];
        let arrOfSuitIcons = ['♦', '♥', '♣', '♠'];
        while (array.length < 5) {
            let identical;
            let randomRank = Math.floor(Math.random() * 13);
            let randomSuit = Math.floor(Math.random() * 4);

            let obj = {
                className: `card rank-${arrOfRank[randomRank].toLowerCase()} ${arrOfSuit[randomSuit]}`,
                suit: arrOfSuitIcons[randomSuit],
                rank: arrOfRank[randomRank]
            };

            array.forEach(function (item) {
                if (item.className === obj.className) {
                    identical = 'identicalCard';
                }
            });
            if (identical !== 'identicalCard') {
                array.push(obj);
            }
        }

        const calc = new WinCalculator(array);
        const outcome = calc.getBestHand();
        this.setState({array, outcome});
    };

    render() {
        return (
            <div className="App playingCards faceImages">
                <button onClick={this.handleClick} id="random">Shuffle Cards</button>
                <ul className="table">
                    {this.state.array.map((obj, i) => <Card class={obj.className} rank={obj.rank} suit={obj.suit} key={i}/>)}
                </ul>
                <p>{this.state.outcome}</p>
            </div>
        );
    }
}


export default App;
